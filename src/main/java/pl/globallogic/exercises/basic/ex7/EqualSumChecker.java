package pl.globallogic.exercises.basic.ex7;

public class EqualSumChecker {

    public static void main(String args[]) {
        System.out.println(hasEqualSum(1,1,1));
        System.out.println(hasEqualSum(1,1,2));
        System.out.println(hasEqualSum(1,-1,0));
    }

    private static boolean hasEqualSum(int numberA, int numberB, int sum) {
        if(numberA+numberB == sum) {
            return true;
        }
        else {
            return false;
        }
    }
}
