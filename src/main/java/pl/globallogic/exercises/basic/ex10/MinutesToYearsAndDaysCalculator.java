package pl.globallogic.exercises.basic.ex10;

public class MinutesToYearsAndDaysCalculator {

    public static void main(String args[]) {
        printYearsAndDays(525600);
        printYearsAndDays(1051200);
        printYearsAndDays(561600);
    }

    private static void printYearsAndDays(long minutes) {

        int years;
        int days;

        if (minutes >= 0) {
            years = Math.round(minutes/(365*24*60));
            days = Math.round(minutes/(24*60)) - years*365;
            System.out.println(minutes + " min = " + years + " y and " + days + " d");
        }
        else {
            System.out.println("Invalid Value");
        }
    }
}
