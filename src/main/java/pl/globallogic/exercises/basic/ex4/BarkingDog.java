package pl.globallogic.exercises.basic.ex4;

public class BarkingDog {

    public static void main(String args[]) {
        System.out.println(shouldWakeUp(true,1));
        System.out.println(shouldWakeUp(false,2));
        System.out.println(shouldWakeUp(true,8));
        System.out.println(shouldWakeUp(true,-1));
    }

    private static boolean shouldWakeUp(boolean barking, int hourOfTheDay) {

        if (hourOfTheDay < 23 && hourOfTheDay > 0) {
            if ((hourOfTheDay > 22 || hourOfTheDay < 8) && barking == true) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}
