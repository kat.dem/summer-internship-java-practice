package pl.globallogic.exercises.basic.ex3;

public class MegaBytesConverter {

    public static void main(String[] args) {
        printMegaBytesAndKiloBytes(2500);
        printMegaBytesAndKiloBytes(-1024);
        printMegaBytesAndKiloBytes(5000);
    }

    private static void printMegaBytesAndKiloBytes(int kiloBytes) {

        int megaBytes = kiloBytes/1024;
        long remainingKiloBytes = kiloBytes - megaBytes*1024;

        if (kiloBytes < 0) {
            System.out.println("Invalid Value");
        }
        else {
            System.out.println(kiloBytes + " KB = " + megaBytes + " MB and " + remainingKiloBytes + " KB");
        }
    }
}
