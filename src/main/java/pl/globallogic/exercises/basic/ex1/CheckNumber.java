package pl.globallogic.exercises.basic.ex1;

import java.util.Scanner;

public class CheckNumber {
    public static void main(String[] args) {
        int number;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number: ");
        number = Integer.parseInt(scanner.next());
        checkNumber(number);
    }

    public static void checkNumber(int number) {
        if (number > 0) {
            System.out.println("positive");
        }
        else if (number < 0) {
            System.out.println("negative");
        }
        else {
            System.out.println("zero");
        }
    }
}
