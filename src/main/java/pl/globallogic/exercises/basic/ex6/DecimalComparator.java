package pl.globallogic.exercises.basic.ex6;

public class DecimalComparator {

    public static void main(String args[]) {
        System.out.println(areEqualByThreeDecimalPlaces(-3.1756, -3.175));
        System.out.println(areEqualByThreeDecimalPlaces(3.175, 3.176));
        System.out.println(areEqualByThreeDecimalPlaces(3.0, 3.0));
        System.out.println(areEqualByThreeDecimalPlaces(-3.123, 3.123));
    }

    private static boolean areEqualByThreeDecimalPlaces(double firstNumber, double secondNumber) {

        double ThreeDecimalPlacesFirstNumber = 0;
        double ThreeDecimalPlacesSecondNumber = 0;

        if (firstNumber > 0 && secondNumber > 0) {
            ThreeDecimalPlacesFirstNumber = Math.floor(firstNumber * 1000.0) / 1000.0;
            ThreeDecimalPlacesSecondNumber = Math.floor(secondNumber * 1000.0) / 1000.0;
        }
        else {
            ThreeDecimalPlacesFirstNumber = Math.ceil(firstNumber * 1000.0) / 1000.0;
            ThreeDecimalPlacesSecondNumber = Math.ceil(secondNumber * 1000.0) / 1000.0;
        }

        if(ThreeDecimalPlacesFirstNumber == ThreeDecimalPlacesSecondNumber) {
            return true;
        }
        else {
            return false;
        }
    }
}
