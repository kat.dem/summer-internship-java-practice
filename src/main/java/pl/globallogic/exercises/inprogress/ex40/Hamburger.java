package pl.globallogic.exercises.inprogress.ex40;

public class Hamburger {
    private String name, addition1Name, addition2Name, addition3Name, addition4Name;
    private String meat;
    private String breadRollType;
    private double price, addition1Price, addition2Price, addition3Price, addition4Price;

    public Hamburger(String name, String meat, String breadRollType, double price) {
        this.name = name;
        this.meat = meat;
        this.breadRollType = breadRollType;
        this.price = price;
    }

    public void addHamburgerAddition1(String name, double price) {
        this.addition1Price = price;
        this.addition1Name = name;
    }

    public void addHamburgerAddition2(String name, double price) {
        this.addition2Price = price;
        this.addition2Name = name;
    }

    public void addHamburgerAddition3(String name, double price) {
        this.addition3Price = price;
        this.addition3Name = name;
    }

    public void addHamburgerAddition4(String name, double price) {
        this.addition4Price = price;
        this.addition4Name = name;
    }

    public double itemizehamburger() {
        if (addition1Name != null) {
            return price += addition1Price;
        }
        if (addition2Name != null) {
            return price += addition2Price;
        }
        if (addition3Name != null) {
            return price += addition3Price;
        }
        if (addition4Name != null) {
            return price += addition4Price;
        }
        return 0;
    }


}
