package pl.globallogic.exercises.inprogress.ex45;

import java.util.ArrayList;

public class Album {
    private String name, artist;
    private ArrayList<Song> songs;

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
    }

    public boolean addSong(String title, double duration) {
        try {
            songs.add(new Song(title, duration));
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

  //  public Song findSong(String title) {

   // }
}
