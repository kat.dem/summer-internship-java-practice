package pl.globallogic.exercises.inprogress.ex48;

import java.util.List;

public interface ISaveable {
    List<String> write();
    void read(List<String> savedValues);
}
