package pl.globallogic.exercises.inprogress.ex39;

public class Car {
    private boolean engine;
    private int cylinders;
    private String name;
    private int wheels;

    public Car(int cylinders, String name) {
        this.cylinders = cylinders;
        this.name = name;
        wheels = 4;
        engine = true;
    }

    private String startEngine() {
        return "start";
    }

    private String accelerate() {
        return "idk";
    }

    private String brake() {
        return "brake";
    }

    public int getCylinders() {
        return cylinders;
    }

    public String getName() {
        return name;
    }


}
