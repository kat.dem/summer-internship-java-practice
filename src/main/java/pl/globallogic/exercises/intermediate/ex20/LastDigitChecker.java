package pl.globallogic.exercises.intermediate.ex20;

public class LastDigitChecker {

    public static void main(String[] args) {
        System.out.println(hasSameLastDigit(41,22,71));
        System.out.println(hasSameLastDigit(23,32,42));
        System.out.println(hasSameLastDigit(9,99,999));
        System.out.println("----------");
        System.out.println(isValid(468));
        System.out.println(isValid(1051));

    }

    private static boolean hasSameLastDigit(int one, int two, int three) {
        if(one >= 10 && one <= 1000 && two >= 10 && two <= 1000 && three >= 10 && three <=1000)
        if(one%10 == two%10 || one%10 == three%10 || two%10 == three%10) {
            return true;
        }
        return false;
    }

    private static boolean isValid(int four) {
        if(four >=10 && four <= 1000) {
            return true;
        }
        return false;
    }
}
