package pl.globallogic.exercises.intermediate.ex13;

import java.util.Scanner;

public class NumberInWord {
    public static void main(String[] args) {
        int number;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number: ");
        number = Integer.parseInt(scanner.next());
        System.out.println(printNumberInWord(number));
    }

    private static String printNumberInWord(int number) {
        switch (number) {
            case 0: return "ZERO";
            case 1: return "ONE";
            case 2: return "TWO";
            case 3: return "THREE";
            case 4: return "FOUR";
            case 5: return "FIVE";
            case 6: return "SIX";
            case 7: return "SEVEN";
            case 8: return "EIGHT";
            case 9: return "NINE";
        }
        return "OTHER";
    }
}
