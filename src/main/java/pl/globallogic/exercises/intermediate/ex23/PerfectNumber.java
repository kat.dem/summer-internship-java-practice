package pl.globallogic.exercises.intermediate.ex23;

public class PerfectNumber {

    public static void main(String[] args) {
        System.out.println(isPerfectNumber(6));
        System.out.println(isPerfectNumber(28));
        System.out.println(isPerfectNumber(5));
        System.out.println(isPerfectNumber(-1));
    }

    private static boolean isPerfectNumber(int number) {
        int sum = 0;
        if(number < 1) {
            return false;
        }
        else {
            int i = 1;
            while (i<number) {
                if (number%i == 0) {
                    sum = sum + i;
                }
                i++;
            }
            if(sum == number) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
