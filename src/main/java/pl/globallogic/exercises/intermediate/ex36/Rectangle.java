package pl.globallogic.exercises.intermediate.ex36;

public class Rectangle {
    private double width;
    private double length;

    public Rectangle(double width, double length) {
        if (this.width < 0) width = 0;
        this.width = width;
        if (this.length < 0) length = 0;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public double getArea() {
        return width*length;
    }
}
