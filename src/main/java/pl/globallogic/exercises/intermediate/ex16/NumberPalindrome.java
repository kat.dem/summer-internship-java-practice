package pl.globallogic.exercises.intermediate.ex16;

public class NumberPalindrome {

    public static void main(String[] args) {
        System.out.println(isPalindrome(-1221));
        System.out.println(isPalindrome(707));
        System.out.println(isPalindrome(11212));
    }

    private static boolean isPalindrome(int number) {
        int num = number;
        int reversed = 0;

        while (number != 0) {
            int last = number % 10;
            reversed = reversed * 10 + last;
            number /= 10;
        }

        if (num == reversed) {
            return true;
        }
        return false;
    }
}
