package pl.globallogic.exercises.intermediate.ex21;

public class AllFactors {

    public static void main(String[] args) {
        printFactors(6);
        printFactors(32);
        printFactors(10);
        printFactors(-1);
    }

    private static void printFactors(int number) {
        if(number < 1) {
            System.out.println("Invalid Value");
        }
        else {
            int i=1;
            while (i<=number) {
                if (number % i == 0) {
                    System.out.print(i + " ");
                }
                i++;
            }
            System.out.println();
        }
    }
}
