package pl.globallogic.exercises.intermediate.ex17;

public class FirstAndLastDigitSum {

    public static void main(String[] args) {
        System.out.println(sumFirstAndLastDigit(252));
        System.out.println(sumFirstAndLastDigit(257));
        System.out.println(sumFirstAndLastDigit(0));
        System.out.println(sumFirstAndLastDigit(5));
        System.out.println(sumFirstAndLastDigit(-10));
    }

    private static int sumFirstAndLastDigit(int number) {
        if (number < 0) {
            return -1;
        }
        int lastNumber = number % 10;
        int firstNumber = 0;

        while (number > 10) {
            number /= 10;
        }
        firstNumber = number;

        return firstNumber + lastNumber;
    }
}
