package pl.globallogic.exercises.intermediate.ex14;

public class NumberOfDaysInMonth {
    public static void main(String[] args) {
       System.out.println(getDaysInMonth(1,2020));
       System.out.println(getDaysInMonth(2,2020));
       System.out.println(getDaysInMonth(2,2018));
       System.out.println(getDaysInMonth(-1,2020));
    }

    private static boolean isLeapYear(int year) {
        if (year >= 1 && year <= 9999) {
            if (year % 4 == 0) {
                if (year % 100 == 0) {
                    if (year % 400 == 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static int getDaysInMonth(int month, int year) {
        boolean leapYear = isLeapYear(year);

        if ((month < 1 && month > 12) || (year < 1 && year > 9999)) {
            return -1;
        }
        else {
            switch (month) {
                case 1, 3, 5, 7, 8, 10, 12:
                    return 31;
                case 2:
                    if (leapYear == false) {
                        return 28;
                    } else
                        return 29;
                case 4, 6, 9, 11:
                    return 30;
            }
        }
        return month;
    }
}
