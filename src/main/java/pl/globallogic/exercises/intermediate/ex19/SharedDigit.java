package pl.globallogic.exercises.intermediate.ex19;

public class SharedDigit {

    public static void main(String[] args) {
        System.out.println(hasSharedDigit(12, 23));
        System.out.println(hasSharedDigit(9, 99));
        System.out.println(hasSharedDigit(15, 55));
    }

    private static boolean hasSharedDigit(int number1, int number2) {
        if (number2 >= 10 && number2 <= 99 && number1 >= 10 && number1 <= 99) {
            int number1a = number1 / 10;
            int number1b = number1 % 10;
            int number2a = number2 / 10;
            int number2b = number2 % 10;

            if (number2a == number1a || number2a == number1b || number2b == number1b || number2b == number1a) {
                return true;
            }
            return false;
        }
            return false;
    }
}
