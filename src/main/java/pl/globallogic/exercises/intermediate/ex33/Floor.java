package pl.globallogic.exercises.intermediate.ex33;

public class Floor {
    private double width;
    private double length;

    public Floor(double width, double length) {
        if(this.width < 0) width = 0;
        this.width = width;
        if(this.length < 0) length = 0;
        this.length = length;
    }

    public double getArea() {
        return width*length;
    }
}