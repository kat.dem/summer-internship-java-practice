package pl.globallogic.exercises.intermediate.ex33;

public class Carpet {
    private double cost;

    public Carpet(double cost) {
        if (this.cost < 0) cost = 0;
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }





}
