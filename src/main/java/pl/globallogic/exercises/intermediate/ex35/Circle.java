package pl.globallogic.exercises.intermediate.ex35;

public class Circle {
    private double radius;

    public Circle(double radius) {
        if(this.radius < 0) radius = 0;
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return getRadius()*getRadius()*Math.PI;
    }
}
