package pl.globallogic.exercises.intermediate.ex22;

public class GreatestCommonDivision {

    public static void main(String[] args) {
        System.out.println(getGreatestCommonDivisor(25,15));
        System.out.println(getGreatestCommonDivisor(12,30));
        System.out.println(getGreatestCommonDivisor(9,18));
        System.out.println(getGreatestCommonDivisor(81,153));
    }

    private static int getGreatestCommonDivisor(int first,int second) {
        int temp = 0;
        if (first < 10 || second < 10) {
            return -1;
        }
        else {
            int min = Math.min(first, second);
            for (int i = 1; i <= min; i++) {
                if (first % i == 0 && second % i == 0) {
                    temp = i;
                }
            }
        }
        return temp;
    }
}
