package pl.globallogic.exercises.intermediate.ex38;

public class Printer {
    private int tonerLevel;
    private int pagesPrinted;
    private boolean duplex;

    public Printer(int tonerLevel, boolean duplex) {
        if (this.tonerLevel >= 0 && this.tonerLevel <= 100) {
            this.tonerLevel = tonerLevel;
        }
        else {
            this.tonerLevel = -1;
        }
        this.pagesPrinted = 0;
        this.duplex = duplex;

    }

    public int addToner(int tonerAmount) {
        if (tonerAmount > 0 && tonerAmount <= 100) {
            if (tonerAmount + tonerLevel > 100) {
                return -1;
            }
            return tonerLevel += tonerAmount;
        }
        return -1;
    }

    public int printPages(int pages) {
        int print = 0;
        if (duplex == true) {
            System.out.println("Printing in duplex mode");
            print = pages/2;
        }
        else {
            print = pages;
        }
        pagesPrinted += print;
       return print;
    }

    public int getPagesPrinted() {
        return pagesPrinted;
    }
}
